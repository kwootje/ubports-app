# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: com.ubuntu.ubports\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-12-11 12:49+0000\n"
"PO-Revision-Date: 2020-07-09 12:16+0000\n"
"Last-Translator: Heimen Stoffels <vistausss@outlook.com>\n"
"Language-Team: Dutch <https://translate.ubports.com/projects/ubports/"
"ubports-app/nl/>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"

#: ../qml/About.qml:34
msgid "About the App"
msgstr "Over de app"

#: ../qml/About.qml:50
msgid "%1 App"
msgstr "%1-app"

#: ../qml/About.qml:64
msgid "Version: "
msgstr "Versie: "

#: ../qml/About.qml:68
msgid ""
"UBports is an international community of developers and users who wish to "
"keep Ubuntu Touch alive. The project was founded by Marius Gripsgard in 2015 "
"as a place where developers can talk and learn from each other and help "
"bring Ubuntu to more devices as a team. After Canonical announced the end of "
"their support for Ubuntu Touch, the operating system was picked up by the "
"community. The response has been fantastic and our community is growing "
"rapidly."
msgstr ""
"UBports is een internationale gemeenschap van ontwikkelaars en gebruikers "
"die er naar streven om Ubuntu Touch door te ontwikkelen. Het project is in "
"2015 gestart door Marius Gripsgard met als doel een plaats te creëren voor "
"ontwikkelaars, waar zij elkaar kunnen helpen en van elkaar kunnen leren om "
"samen Ubuntu naar meer apparaten te brengen. Nadat Canonical aangaf te "
"stoppen met de ondersteuning van Ubuntu Touch, is het besturingssysteem "
"overgenomen door onze gemeenschap. De gemeenschap groeit snel en werkt er "
"met veel plezier aan."

#: ../qml/About.qml:72
msgid ""
"It's our goal to keep Ubuntu Touch alive on all existing devices. While "
"keeping all its good and beautiful parts, we want to reshape Ubuntu Touch to "
"make it the operating system we want to use on a daily basis. Ubuntu is "
"Linux for human beings, and Ubuntu Touch brings this spirit to mobile "
"devices. We want to turn Ubuntu Touch into a true Linux distribution for "
"mobile devices, while keeping its simple and elegant design."
msgstr ""
"Ons doel is om Ubuntu Touch te blijven ondersteunen op alle bestaande "
"apparaten. Wij willen alle goede onderdelen uit Ubuntu Touch bewaren, maar "
"ze ook verbeteren om het systeem geschikt te maken voor dagelijks gebruik. "
"Ubuntu is \"Linux voor gewone mensen\", en Ubuntu Touch brengt deze gedachte "
"over naar mobiele apparaten. Wij willen Ubuntu Touch omtoveren tot een "
"volwaardige Linux-distributie voor mobiele apparaten, waarbij het simpele en "
"elegante ontwerp behouden blijft."

#. TRANSLATORS: A localized URL can be used for the license
#: ../qml/About.qml:77
msgid ""
"This program is free software: you can redistribute it and/or modify it "
"under the terms of the GNU General Public License as published by the Free "
"Software Foundation, either version 3 of the License, or (at your option) "
"any later version. This program is distributed in the hope that it will be "
"useful, but WITHOUT ANY WARRANTY; without even the implied warranty of "
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the <a "
"href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU General Public "
"License</a> for more details."
msgstr ""
"Deze app is vrije software: je kunt het opnieuw uitbrengen en/of wijzigen "
"onder de voorwaarden van de GNU General Public License, zoals gepubliceerd "
"door de Free Software Foundation, ofwel versie 3 van de licentie, of (naar "
"keuze) elke latere versie. Dit programma wordt verspreid in de hoop dat het "
"nuttig zal zijn, maar ZONDER ENIGE GARANTIE; zelfs zonder de impliciete "
"garantie van VERKOOPBAARHEID of GESCHIKTHEID VOOR EEN BEPAALD DOEL. Zie de <"
"a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU General Public "
"License</a> voor meer informatie."

#: ../qml/About.qml:81
msgid "SOURCE"
msgstr "BRON"

#: ../qml/About.qml:81
msgid "ISSUES"
msgstr "PROBLEMEN"

#: ../qml/About.qml:81
msgid "DONATE"
msgstr "DONEREN"

#: ../qml/About.qml:85
msgid "Copyright (c) 2017 UBports <ubports.com>"
msgstr "Copyright (c) 2017 UBports <ubports.com>"

#: ../qml/About.qml:89
msgid "Maintained by"
msgstr "Onderhouden door"

#: ../qml/Home.qml:31 ../qml/Home.qml:53 ../qml/Main.qml:32
msgid "Welcome to %1!"
msgstr "Welkom bij %1!"

#: ../qml/Home.qml:58
msgid "We can change the future! Let's innovate and dream again."
msgstr "We kunnen de toekomst veranderen! Laten we weer innoveren en dromen."

#: ../qml/Home.qml:63
msgid ""
"UBports builds the most private and innovative experience. It is limited in "
"potential only by what you - the community member - can dream. Regain "
"control of your device and personal data. You have choice and freedom with "
"UBports."
msgstr ""
"UBports bouwt aan de persoonlijkste en innovatiefste ervaring. Het wordt "
"alleen beperkt in mogelijkheden die jij - het gemeenschapslid - kunt dromen. "
"Krijg weer controle over je apparaat en persoonlijke gegevens - met UBports "
"heb je keuze en vrijheid."

#: ../qml/Home.qml:67
msgid "Be more than a spectator - Become a game changer!"
msgstr "Wees meer dan een toeschouwer: word een vernieuwer!"

#. TRANSLATORS: Please make sure the URLs are correct and don't fuck up the HTML.
#: ../qml/Home.qml:73
msgid ""
"With UBports you can actually change the things you don't like. Ever felt "
"frustrated by a feature or a limitation or a weird layout? Get it changed! "
"By <a href='https://github.com/ubports/ubports-touch'>reporting bugs and "
"sending feature requests</a> UBports will become the smartest platform "
"you've ever experienced."
msgstr ""
"Herken je dat gevoel, dat iets te beperkt is of er niet uitziet? Geen "
"probleem; pas het gewoon aan! Met UBports kun je alles wat je niet aanstaat "
"aanpassen. Door <a href='https://github.com/ubports/ubports-touch'>bugs te "
"melden en ideeën in te dienen </a>, wordt UBports het innovatiefste platform "
"dat je ooit hebt gebruikt."

#: ../qml/Home.qml:77
msgid "Pay what's fair"
msgstr "Betaal een redelijke prijs"

#. TRANSLATORS: Please make sure the URLs are correct and don't fuck up the HTML.
#: ../qml/Home.qml:83
msgid ""
"UBports was created and exists because of many volunteers in the community "
"near you and all over the world. Although we don't ask for a fixed fee we do "
"depend on financial support from the community. So if you like Ubuntu Touch "
"and want to see the project succeed, please consider a <a href='https://"
"paypal.me/ubports'>one-time</a> or <a href='https://patreon.com/"
"ubports'>regular monthly</a> or <a href='https://liberapay.com/"
"ubports'>weekly</a> donation. We sincerely appreciate it and we know that "
"you'll feel great about your fair contribution and be excited to particpate. "
"In return, we believe in providing fair sevice and opportunity with "
"transparency to all members of the project."
msgstr ""
"UBports is gemaakt en bestaat door veel vrijwilligers in de gemeenschap in "
"jouw buurt en over heel de wereld. We vragen geen vaste vergoeding en zijn "
"dus afhankelijk van financiële steun uit de gemeenschap. Als je Ubuntu Touch "
"graag gebruikt en het wil zien slagen, overweeg dan alsjeblieft een <a "
"href='https://paypal.me/ubports'>eenmalige</a>, <a href='https://patreon.com/"
"ubports'>maandelijkse</a> of <a href='https://liberapay.com/"
"ubports'>wekelijkse</a> donatie. Wij stellen het zeer op prijs en weten dat "
"je je goed voelt over je bijdrage en enthousiast bent om deel te nemen. In "
"ruil daarvoor geloven wij in het verstrekken van goede dienstverlening en "
"mogelijkheden met openheid naar alle leden van het project."

#: ../qml/Home.qml:87
msgid "Get looped in"
msgstr "Raak betrokken"

#. TRANSLATORS: Please make sure the URLs are correct and don't fuck up the HTML.
#: ../qml/Home.qml:93
msgid ""
"We are in this together. As a starting point, stay on top of the latest "
"developments, related news and a whole bunch of other experience-enhancing "
"updates. <a href='https://list.ubports.com/mailman/listinfo'>Join our e-mail "
"newsletter</a> to stay in the loop."
msgstr ""
"We staan hier samen in. Om te beginnen kun je op de hoogte blijven van de "
"laatste ontwikkelingen, gerelateerd nieuws en een hele hoop andere "
"ervaringsverbeterende updates. <a href='https://list.ubports.com/mailman/"
"listinfo'>Abonneer je op onze mailinglijst</a> om op de hoogte te blijven."

#. TRANSLATORS: Please make sure the URLs are correct and don't fuck up the HTML.
#: ../qml/Home.qml:98
msgid ""
"UBports is made possible by our <a href='https://ubports.com/sponsors'>our "
"awesome sponsors, donors and patrons</a>!"
msgstr ""
"UBports wordt mogelijk gemaakt door onze <a href='https://ubports.com/"
"sponsors'>geweldige sponsors, donateurs en patrons</a>!"

#. TRANSLATORS: Description of the menu item
#: ../qml/modules/DefaultHeader.qml:37
msgid "About"
msgstr "Over"

#. TRANSLATORS: Description of the menu item
#: ../qml/modules/DefaultHeader.qml:50
msgid "Support Forum"
msgstr "Ondersteuningsforum"

#. TRANSLATORS: Description of the menu item
#: ../qml/modules/DefaultHeader.qml:55
msgid "Get involved"
msgstr "Raak betrokken"

#. TRANSLATORS: Description of the menu item
#: ../qml/modules/DefaultHeader.qml:60
msgid "Newsletter"
msgstr "Nieuwsbrief"

#: com.ubuntu.ubports.desktop.in.h:1
msgid "UBports"
msgstr "UBports"

#~ msgid "Welcome to UBports!"
#~ msgstr "Welkom bij UBports!"

#~ msgid "UBports App"
#~ msgstr "UBports App"

#, fuzzy
#~ msgid "Meet The Team"
#~ msgstr "Ontmoet het team"

#~ msgid "Devices"
#~ msgstr "Apparaten"

#~ msgid "Maintained by Jan Sprinz <neo@neothethird.de>"
#~ msgstr "Onderhouden door Jan Sprinz <neo@neothethird.de>"
